<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>E - LogBook</title>

    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-96x96.png">

    <link rel="stylesheet" href="lib/ionicons.css">
    <link rel="stylesheet" href="lib/angular-toastr.css">
    <link rel="stylesheet" href="lib/animate.css">
    <link rel="stylesheet" href="lib/bootstrap.css">
    <link rel="stylesheet" href="lib/bootstrap-select.css">
    <link rel="stylesheet" href="lib/bootstrap-switch.css">
    <link rel="stylesheet" href="lib/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="lib/font-awesome.css">
    <link rel="stylesheet" href="lib/angular-progress-button-styles.min.css">
    <link rel="stylesheet" href="lib/textAngular.css">
    <link rel="stylesheet" href="lib/xeditable.css">
    <link rel="stylesheet" href="lib/style.css">
    <link rel="stylesheet" href="lib/main.css">
</head>
<body>
<main class="menu-collapsed">
    <page-top>
        <div class="page-top clearfix ng-isolate-scope scrolled" scroll-position="scrolled" max-height="50">
            <a href="#" class="al-logo clearfix"><span>E </span>- LogBook</a>

            <div class="search">
                <i class="ion-ios-search-strong" ></i>
                <input id="searchInput" type="text" placeholder="Search for...">
            </div>

            <div class="user-profile clearfix">
                <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                    <div class="al-user-profile dropdown" id="profile_menu">
                    <a id="profile-toggle" class="profile-toggle-link dropdown-toggle" aria-haspopup="true" aria-expanded="true">
                        <img src="assets/img/app/profile/Kostya.png">
                    </a>


                </div>
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                นายประวัติ ธนะราช <b class="caret"></b></a>
                            <ul class="dropdown-menu animated fadeInUp">
                                <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href=""><i class="fa fa-cog"></i> Settings</a></li>
                                <li><a href="" class="signout"><i class="fa fa-power-off"></i> Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                    </nav>

                </nav>
                <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shift Chart <b class="caret"></b></a>
                            <ul class="dropdown-menu animated fadeInUp">
                                <li><a href=""><i class="fa fa-plus-square"></i> ป้อนข้อมูล</a></li>
                                <li><a href=""><i class="fa fa-check-square"></i> ตรวจสอบ</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Operator <b class="caret"></b></a>
                            <ul class="dropdown-menu animated fadeInUp">
                                <li><a href=""><i class="fa fa-plus-square"></i> ป้อนข้อมูล</a></li>
                                <li><a href=""><i class="fa fa-check-square"></i> ตรวจสอบ</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </page-top>
    <div class="al-main" id="main">
        <!--start content here -->
        <div class="al-content">
            <content-top>
                <div class="content-top clearfix">
                    <h1 class="al-title ng-binding">Form Inputs</h1>

                    <ul class="breadcrumb al-breadcrumb">
                        <li>
                            <a href="#">Home</a></li>
                        <li class="ng-binding">Form Inputs</li>
                    </ul>
                </div>
            </content-top>
            <div class="row">
                <div class="col-md-6">
                    <div ba-panel-title="Standard Fields" ba-panel-class="with-scroll">
                        <div class="panel  with-scroll animated zoomIn" zoom-in="">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title">Standard Fields</h3>
                            </div>
                            <div class="panel-body">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/standardFields.html' -->
                                <div ng-include="'app/pages/form/inputs/widgets/standardFields.html'" class="ng-scope" style="">
                                    <form class="ng-pristine ng-valid ng-scope">
                                        <div class="form-group">
                                            <label for="input01">Text</label>
                                            <input type="text" class="form-control" id="input01" placeholder="Text">
                                        </div>
                                        <div class="form-group">
                                            <label for="input02">Password</label>
                                            <input type="password" class="form-control" id="input02" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="input03">Rounded Corners</label>
                                            <input type="text" class="form-control form-control-rounded" id="input03" placeholder="Rounded Corners">
                                        </div>
                                        <div class="form-group">
                                            <label for="input04">With help</label>
                                            <input type="text" class="form-control" id="input04" placeholder="With help">
                                            <span class="help-block sub-little-text">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="input05">Disabled Input</label>
                                            <input type="text" class="form-control" id="input05" placeholder="Disabled Input" disabled="">
                                        </div>

                                        <div class="form-group">
                                            <label for="textarea01">Textarea</label>
                                            <textarea placeholder="Default Input" class="form-control" id="textarea01"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="input2" placeholder="Small Input">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-lg" id="input4" placeholder="Large Input">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ba-panel-title="Tags Input" ba-panel-class="with-scroll">
                        <div class="panel  with-scroll animated zoomIn" zoom-in="">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title">Tags Input</h3>
                            </div>
                            <div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/tagsInput/tagsInput.html' --><div ng-include="'app/pages/form/inputs/widgets/tagsInput/tagsInput.html'" class="ng-scope" style=""><div class="form-group ng-scope">
                                        <div class="form-group">
                                            <div class="bootstrap-tagsinput"><span class="tag label label-primary">Amsterdam<span data-role="remove"></span></span> <span class="tag label label-primary">Washington<span data-role="remove"></span></span> <span class="tag label label-primary">Sydney<span data-role="remove"></span></span> <span class="tag label label-primary">Beijing<span data-role="remove"></span></span> <span class="tag label label-primary">Cairo<span data-role="remove"></span></span> <input type="text" placeholder="Add Tag"></div><input type="text" tag-input="primary" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" placeholder="Add Tag" style="display: none;">
                                        </div>
                                        <div class="form-group">
                                            <div class="bootstrap-tagsinput"><span class="tag label label-warning">Minsk<span data-role="remove"></span></span> <span class="tag label label-warning">Prague<span data-role="remove"></span></span> <span class="tag label label-warning">Vilnius<span data-role="remove"></span></span> <span class="tag label label-warning">Warsaw<span data-role="remove"></span></span> <input type="text" placeholder="Add Tag"></div><input type="text" tag-input="warning" value="Minsk,Prague,Vilnius,Warsaw" data-role="tagsinput" placeholder="Add Tag" style="display: none;">
                                        </div>
                                        <div class="form-group">
                                            <div class="bootstrap-tagsinput"><span class="tag label label-danger">London<span data-role="remove"></span></span> <span class="tag label label-danger">Berlin<span data-role="remove"></span></span> <span class="tag label label-danger">Paris<span data-role="remove"></span></span> <span class="tag label label-danger">Rome<span data-role="remove"></span></span> <span class="tag label label-danger">Munich<span data-role="remove"></span></span> <input type="text" placeholder="Add Tag"></div><input type="text" tag-input="danger" value="London,Berlin,Paris,Rome,Munich" data-role="tagsinput" placeholder="Add Tag" style="display: none;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ba-panel="" ba-panel-title="Input Groups" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Input Groups</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/inputGroups.html' --><div ng-include="'app/pages/form/inputs/widgets/inputGroups.html'" class="ng-scope" style=""><div class="input-group ng-scope">
                                        <span class="input-group-addon input-group-addon-primary addon-left" id="basic-addon1">@</span>
                                        <input type="text" class="form-control with-primary-addon" placeholder="Username" aria-describedby="basic-addon1">
                                    </div>

                                    <div class="input-group ng-scope">
                                        <input type="text" class="form-control with-warning-addon" placeholder="Recipient's username" aria-describedby="basic-addon2">
                                        <span class="input-group-addon input-group-addon-warning addon-right" id="basic-addon2">@example.com</span>
                                    </div>

                                    <div class="input-group ng-scope">
                                        <span class="input-group-addon addon-left input-group-addon-success">$</span>
                                        <input type="text" class="form-control with-success-addon" aria-label="Amount (to the nearest dollar)">
                                        <span class="input-group-addon addon-right input-group-addon-success">.00</span>
                                    </div>

                                    <div class="input-group ng-scope">
                                        <input type="text" class="form-control with-danger-addon" placeholder="Search for...">
                                        <span class="input-group-btn">
      <button class="btn btn-danger" type="button">Go!</button>
  </span>
                                    </div></div>
                            </div></div></div>
                    <div ba-panel="" ba-panel-title="Checkboxes &amp; Radios" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Checkboxes &amp; Radios</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/checkboxesRadios.html' --><div ng-include="'app/pages/form/inputs/widgets/checkboxesRadios.html'" class="ng-scope" style=""><div class="checkbox-demo-row ng-scope">
                                        <div class="input-demo checkbox-demo row">
                                            <div class="col-md-4">
                                                <label class="checkbox-inline custom-checkbox nowrap">
                                                    <input type="checkbox" id="inlineCheckbox01" value="option1">
                                                    <span>Check 1</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline custom-checkbox nowrap">
                                                    <input type="checkbox" id="inlineCheckbox02" value="option2">
                                                    <span>Check 2</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="checkbox-inline custom-checkbox nowrap">
                                                    <input type="checkbox" id="inlineCheckbox03" value="option3">
                                                    <span>Check 3</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="input-demo radio-demo row">
                                            <div class="col-md-4">
                                                <label class="radio-inline custom-radio nowrap">
                                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                    <span>Option 1</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="radio-inline custom-radio nowrap">
                                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                    <span>Option 2</span>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="radio-inline custom-radio nowrap">
                                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                                    <span>Option3</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ng-scope">
                                        <div class="checkbox disabled">
                                            <label class="custom-checkbox nowrap">
                                                <input type="checkbox" value="" disabled="">
                                                <span>Checkbox is disabled</span>
                                            </label>
                                        </div>
                                        <div class="radio disabled">
                                            <label class="custom-radio nowrap">
                                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled="">
                                                <span>Disabled option</span>
                                            </label>
                                        </div>
                                    </div></div>
                            </div></div></div>
                </div>
                <div class="col-md-6">
                    <div ba-panel="" ba-panel-title="Validation States" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Validation States</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/validationStates.html' --><div ng-include="'app/pages/form/inputs/widgets/validationStates.html'" class="ng-scope" style=""><div class="form-group has-success ng-scope">
                                        <label class="control-label" for="inputSuccess1">Input with success</label>
                                        <input type="text" class="form-control" id="inputSuccess1">
                                    </div>
                                    <div class="form-group has-warning ng-scope">
                                        <label class="control-label" for="inputWarning1">Input with warning</label>
                                        <input type="text" class="form-control" id="inputWarning1">
                                    </div>
                                    <div class="form-group has-error ng-scope">
                                        <label class="control-label" for="inputError1">Input with error</label>
                                        <input type="text" class="form-control" id="inputError1">
                                    </div>
                                    <div class="has-success ng-scope">
                                        <div class="checkbox">
                                            <label class="custom-checkbox">
                                                <input type="checkbox" id="checkboxSuccess" value="option1">
                                                <span>Checkbox with success</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="has-warning ng-scope">
                                        <div class="checkbox">
                                            <label class="custom-checkbox">
                                                <input type="checkbox" id="checkboxWarning" value="option1">
                                                <span>Checkbox with warning</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="has-error ng-scope">
                                        <div class="checkbox">
                                            <label class="custom-checkbox">
                                                <input type="checkbox" id="checkboxError" value="option1">
                                                <span>Checkbox with error</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group has-success has-feedback ng-scope">
                                        <label class="control-label" for="inputSuccess2">Input with success</label>
                                        <input type="text" class="form-control" id="inputSuccess2" aria-describedby="inputSuccess2Status">
                                        <i class="ion-checkmark-circled form-control-feedback" aria-hidden="true"></i>
                                        <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                    </div>
                                    <div class="form-group has-warning has-feedback ng-scope">
                                        <label class="control-label" for="inputWarning2">Input with warning</label>
                                        <input type="text" class="form-control" id="inputWarning2" aria-describedby="inputWarning2Status">
                                        <i class="ion-alert-circled form-control-feedback" aria-hidden="true"></i>
                                        <span id="inputWarning2Status" class="sr-only">(warning)</span>
                                    </div>
                                    <div class="form-group has-error has-feedback ng-scope">
                                        <label class="control-label" for="inputError2">Input with error</label>
                                        <input type="text" class="form-control" id="inputError2" aria-describedby="inputError2Status">
                                        <i class="ion-android-cancel form-control-feedback" aria-hidden="true"></i>
                                        <span id="inputError2Status" class="sr-only">(error)</span>
                                    </div>
                                    <div class="form-group has-success has-feedback ng-scope">
                                        <label class="control-label" for="inputGroupSuccess1">Input group with success</label>
                                        <div class="input-group">
                                            <span class="input-group-addon addon-left">@</span>
                                            <input type="text" class="form-control" id="inputGroupSuccess1" aria-describedby="inputGroupSuccess1Status">
                                        </div>
                                        <i class="ion-checkmark-circled form-control-feedback" aria-hidden="true"></i>
                                        <span id="inputGroupSuccess1Status" class="sr-only">(success)</span>
                                    </div></div>
                            </div></div></div>
                    <div ba-panel="" ba-panel-title="Selects" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Selects</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/select/select.html' --><div ng-include="'app/pages/form/inputs/widgets/select/select.html'" class="ng-scope" style=""><div ng-controller="SelectpickerPanelCtrl as selectpickerVm" class="ng-scope">
                                        <div class="form-group">
                                            <div class="btn-group bootstrap-select form-control ng-pristine ng-untouched ng-valid"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Standard Select"><span class="filter-option pull-left">Standard Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control selectpicker ng-pristine ng-untouched ng-valid" selectpicker="" title="Standard Select" ng-model="selectpickerVm.standardSelected" ng-options="item as item.label for item in selectpickerVm.standardSelectItems" tabindex="-98"><option data-hidden="true" disabled="" value="" class="" selected="selected">Standard Select</option><option class="bs-title-option" value="">Standard Select</option><option label="Option 1" value="object:592">Option 1</option><option label="Option 2" value="object:593">Option 2</option><option label="Option 3" value="object:594">Option 3</option><option label="Option 4" value="object:595">Option 4</option></select></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="btn-group bootstrap-select form-control with-search ng-pristine ng-untouched ng-valid"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Select With Search"><span class="filter-option pull-left">Select With Search</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"></div><ul class="dropdown-menu inner" role="menu"><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Hot Dog, Fries and a Soda</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Burger, Shake and a Smile</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Sugar, Spice and all things nice</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Baby Back Ribs</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control selectpicker with-search ng-pristine ng-untouched ng-valid" data-live-search="true" title="Select With Search" selectpicker="" ng-model="selectpickerVm.searchSelectedItem" ng-options="item as item.label for item in selectpickerVm.selectWithSearchItems" tabindex="-98"><option data-hidden="true" disabled="" value="" class="" selected="selected">Select With Search</option><option class="bs-title-option" value="">Select With Search</option><option label="Hot Dog, Fries and a Soda" value="object:596">Hot Dog, Fries and a Soda</option><option label="Burger, Shake and a Smile" value="object:597">Burger, Shake and a Smile</option><option label="Sugar, Spice and all things nice" value="object:598">Sugar, Spice and all things nice</option><option label="Baby Back Ribs" value="object:599">Baby Back Ribs</option></select></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Option Types"><span class="filter-option pull-left">Option Types</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Standard option</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option with subtext<small class="text-muted">option subtext</small></span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="glyphicon glyphicon-heart"></span> <span class="text">Option with cion</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control selectpicker" title="Option Types" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Option Types</option>
                                                    <option value="Standard option">Standard option</option>
                                                    <option data-subtext="option subtext" value="Option with subtext">Option with subtext</option>
                                                    <option disabled="" value="Disabled Option">Disabled Option</option>
                                                    <option data-icon="glyphicon-heart" value="Option with cion">Option with cion</option>
                                                    <option data-hidden="true" disabled="" value="">Option Types</option></select></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="btn-group bootstrap-select disabled form-control"><button type="button" class="btn dropdown-toggle disabled btn-default" data-toggle="dropdown" tabindex="-1" title="Disabled Select"><span class="filter-option pull-left">Disabled Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control selectpicker" disabled="" title="Disabled Select" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Disabled Select</option>
                                                    <option value="Option 1">Option 1</option>
                                                    <option value="Option 2">Option 2</option>
                                                    <option value="Option 3">Option 3</option>
                                                    <option data-hidden="true" disabled="" value="">Disabled Select</option></select></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control ng-pristine ng-untouched ng-valid"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Select with Option Groups"><span class="filter-option pull-left">Select with Option Groups</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li class="dropdown-header " data-optgroup="1"><span class="text">Group 1</span></li><li data-original-index="2" data-optgroup="1"><a tabindex="0" class="opt  " style="" data-tokens="null"><span class="text">Group 1 - Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3" data-optgroup="1"><a tabindex="0" class="opt  " style="" data-tokens="null"><span class="text">Group 1 - Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li class="divider" data-optgroup="2div"></li><li class="dropdown-header " data-optgroup="2"><span class="text">Group 2</span></li><li data-original-index="4" data-optgroup="2"><a tabindex="0" class="opt  " style="" data-tokens="null"><span class="text">Group 2 - Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5" data-optgroup="2"><a tabindex="0" class="opt  " style="" data-tokens="null"><span class="text">Group 2 - Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control ng-pristine ng-untouched ng-valid" title="Select with Option Groups" selectpicker="" ng-model="selectpickerVm.groupedSelectedItem" ng-options="item as item.label group by item.group for item in selectpickerVm.groupedSelectItems" tabindex="-98"><option data-hidden="true" disabled="" value="" class="" selected="selected">Select with Option Groups</option><option class="bs-title-option" value="">Select with Option Groups</option><optgroup label="Group 1"><option label="Group 1 - Option 1" value="object:600">Group 1 - Option 1</option><option label="Group 1 - Option 3" value="object:602">Group 1 - Option 3</option></optgroup><optgroup label="Group 2"><option label="Group 2 - Option 2" value="object:601">Group 2 - Option 2</option><option label="Group 2 - Option 4" value="object:603">Group 2 - Option 4</option></optgroup></select></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Select with Divider"><span class="filter-option pull-left">Select with Divider</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Group 1 - Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Group 1 - Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li class="divider" data-original-index="3"></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Group 2 - Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Group 2 - Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Select with Divider" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Select with Divider</option>
                                                            <option value="Group 1 - Option 1">Group 1 - Option 1</option>
                                                            <option value="Group 1 - Option 2">Group 1 - Option 2</option>
                                                            <option data-divider="true" value=""></option>
                                                            <option value="Group 2 - Option 1">Group 2 - Option 1</option>
                                                            <option value="Group 2 - Option 2">Group 2 - Option 2</option>
                                                            <option data-hidden="true" disabled="" value="">Select with Divider</option></select></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="btn-group bootstrap-select show-tick form-control ng-pristine ng-untouched ng-valid"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Multiple Select"><span class="filter-option pull-left">Multiple Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control ng-pristine ng-untouched ng-valid" title="Multiple Select" multiple="" selectpicker="" ng-model="selectpickerVm.multipleSelectedItems" ng-options="item as item.label for item in selectpickerVm.standardSelectItems" tabindex="-98"><option data-hidden="true" disabled="" value="" class="">Multiple Select</option><option label="Option 1" value="object:592">Option 1</option><option label="Option 2" value="object:593">Option 2</option><option label="Option 3" value="object:594">Option 3</option><option label="Option 4" value="object:595">Option 4</option></select></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="btn-group bootstrap-select show-tick form-control ng-pristine ng-untouched ng-valid"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="Multiple Select with Limit"><span class="filter-option pull-left">Multiple Select with Limit</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control ng-pristine ng-untouched ng-valid" title="Multiple Select with Limit" multiple="" data-max-options="2" selectpicker="" ng-model="selectpickerVm.multipleSelectedItems2" ng-options="item as item.label for item in selectpickerVm.standardSelectItems" tabindex="-98"><option data-hidden="true" disabled="" value="" class="">Multiple Select with Limit</option><option label="Option 1" value="object:592">Option 1</option><option label="Option 2" value="object:593">Option 2</option><option label="Option 3" value="object:594">Option 3</option><option label="Option 4" value="object:595">Option 4</option></select></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-primary" data-toggle="dropdown" title="Primary Select"><span class="filter-option pull-left">Primary Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Primary Select" data-style="btn-primary" data-container="body" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Primary Select</option>
                                                            <option value="Option 1">Option 1</option>
                                                            <option value="Option 2">Option 2</option>
                                                            <option value="Option 3">Option 3</option>
                                                            <option value="Option 4">Option 4</option>
                                                            <option data-hidden="true" disabled="" value="">Primary Select</option></select></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-success" data-toggle="dropdown" title="Success Select"><span class="filter-option pull-left">Success Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Success Select" data-style="btn-success" data-container="body" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Success Select</option>
                                                            <option value="Option 1">Option 1</option>
                                                            <option value="Option 2">Option 2</option>
                                                            <option value="Option 3">Option 3</option>
                                                            <option value="Option 4">Option 4</option>
                                                            <option data-hidden="true" disabled="" value="">Success Select</option></select></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-warning" data-toggle="dropdown" title="Warning Select"><span class="filter-option pull-left">Warning Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Warning Select" data-style="btn-warning" data-container="body" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Warning Select</option>
                                                            <option value="Option 1">Option 1</option>
                                                            <option value="Option 2">Option 2</option>
                                                            <option value="Option 3">Option 3</option>
                                                            <option value="Option 4">Option 4</option>
                                                            <option data-hidden="true" disabled="" value="">Warning Select</option></select></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-info" data-toggle="dropdown" title="Info Select"><span class="filter-option pull-left">Info Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Info Select" data-style="btn-info" data-container="body" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Info Select</option>
                                                            <option value="Option 1">Option 1</option>
                                                            <option value="Option 2">Option 2</option>
                                                            <option value="Option 3">Option 3</option>
                                                            <option value="Option 4">Option 4</option>
                                                            <option data-hidden="true" disabled="" value="">Info Select</option></select></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-danger" data-toggle="dropdown" title="Danger Select"><span class="filter-option pull-left">Danger Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Danger Select" data-style="btn-danger" data-container="body" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Danger Select</option>
                                                            <option value="Option 1">Option 1</option>
                                                            <option value="Option 2">Option 2</option>
                                                            <option value="Option 3">Option 3</option>
                                                            <option value="Option 4">Option 4</option>
                                                            <option data-hidden="true" disabled="" value="">Danger Select</option></select></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-inverse" data-toggle="dropdown" title="Inverse Select"><span class="filter-option pull-left">Inverse Select</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Option 4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control" title="Inverse Select" data-style="btn-inverse" data-container="body" selectpicker="" tabindex="-98"><option class="bs-title-option" value="">Inverse Select</option>
                                                            <option value="Option 1">Option 1</option>
                                                            <option value="Option 2">Option 2</option>
                                                            <option value="Option 3">Option 3</option>
                                                            <option value="Option 4">Option 4</option>
                                                            <option data-hidden="true" disabled="" value="">Inverse Select</option></select></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></div></div>
                    <div ba-panel="" ba-panel-title="On/Off Switches" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">On/Off Switches</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/form/inputs/widgets/switch/switch.html' --><div ng-include="'app/pages/form/inputs/widgets/switch/switch.html'" class="ng-scope" style=""><div ng-controller="SwitchPanelCtrl as switchPanelVm" class="switches clearfix ng-scope">
                                        <div class="switch-container primary ng-isolate-scope ng-valid" color="primary" ng-model="switchPanelVm.switcherValues.primary"><div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-small bootstrap-switch-animate" style="width: 84px;"><div class="bootstrap-switch-container" style="width: 123px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-primary" style="width: 41px;">ON</span><span class="bootstrap-switch-label" style="width: 41px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 41px;">OFF</span><input type="checkbox" ng-model="ngModel" class="ng-pristine ng-untouched ng-valid"></div></div></div>
                                        <div class="switch-container warning ng-isolate-scope ng-valid" color="warning" ng-model="switchPanelVm.switcherValues.warning"><div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-small bootstrap-switch-animate" style="width: 84px;"><div class="bootstrap-switch-container" style="width: 123px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-warning" style="width: 41px;">ON</span><span class="bootstrap-switch-label" style="width: 41px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 41px;">OFF</span><input type="checkbox" ng-model="ngModel" class="ng-pristine ng-untouched ng-valid"></div></div></div>
                                        <div class="switch-container danger ng-isolate-scope ng-valid" color="danger" ng-model="switchPanelVm.switcherValues.danger"><div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-small bootstrap-switch-animate" style="width: 84px;"><div class="bootstrap-switch-container" style="width: 123px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-danger" style="width: 41px;">ON</span><span class="bootstrap-switch-label" style="width: 41px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 41px;">OFF</span><input type="checkbox" ng-model="ngModel" class="ng-pristine ng-untouched ng-valid"></div></div></div>
                                        <div class="switch-container info ng-isolate-scope ng-valid" color="info" ng-model="switchPanelVm.switcherValues.info"><div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-small bootstrap-switch-animate" style="width: 84px;"><div class="bootstrap-switch-container" style="width: 123px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-info" style="width: 41px;">ON</span><span class="bootstrap-switch-label" style="width: 41px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 41px;">OFF</span><input type="checkbox" ng-model="ngModel" class="ng-pristine ng-untouched ng-valid"></div></div></div>
                                        <div class="switch-container success ng-isolate-scope ng-valid" color="success" ng-model="switchPanelVm.switcherValues.success"><div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-small bootstrap-switch-animate" style="width: 84px;"><div class="bootstrap-switch-container" style="width: 123px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 41px;">ON</span><span class="bootstrap-switch-label" style="width: 41px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-default" style="width: 41px;">OFF</span><input type="checkbox" ng-model="ngModel" class="ng-pristine ng-untouched ng-valid"></div></div></div>
                                    </div></div>
                            </div></div></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" ba-panel="" ba-panel-title="Flat Buttons" ba-panel-class="with-scroll button-panel"><div class="panel  with-scroll button-panel animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Flat Buttons</h3></div><div class="panel-body" ng-transclude="">
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-default">Default</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-primary">Primary</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-success">Success</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-info">Info</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-warning">Warning</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-danger">Danger</button></div>
                        </div></div></div>
                <div class="col-md-3" ba-panel="" ba-panel-title="Raised Buttons" ba-panel-class="with-scroll button-panel"><div class="panel  with-scroll button-panel animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Raised Buttons</h3></div><div class="panel-body" ng-transclude="">
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-default btn-raised">Default</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-primary btn-raised">Primary</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-success btn-raised">Success</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-info btn-raised">Info</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-warning btn-raised">Warning</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-danger btn-raised">Danger</button></div>
                        </div></div></div>
                <div class="col-md-3" ba-panel="" ba-panel-title="Different sizes" ba-panel-class="with-scroll button-panel df-size-button-panel"><div class="panel  with-scroll button-panel df-size-button-panel animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Different sizes</h3></div><div class="panel-body" ng-transclude="">
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-default  btn-xs">Default</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-primary  btn-sm">Primary</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-success btn-mm">Success</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-info btn-md">Info</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-warning btn-xm">Warning</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-danger btn-lg">Danger</button></div>
                        </div></div></div>
                <div class="col-md-3" ba-panel="" ba-panel-title="Disabled" ba-panel-class="with-scroll button-panel"><div class="panel  with-scroll button-panel animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Disabled</h3></div><div class="panel-body" ng-transclude="">
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-default" disabled="disabled">Default</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-primary" disabled="disabled">Primary</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-success" disabled="disabled">Success</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-info" disabled="disabled">Info</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-warning" disabled="disabled">Warning</button></div>
                            <div class="button-wrapper ng-scope"><button type="button" class="btn btn-danger" disabled="disabled">Danger</button></div>
                        </div></div></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div ba-panel="" ba-panel-title="Icon Buttons" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Icon Buttons</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/ui/buttons/widgets/iconButtons.html' --><div ng-include="'app/pages/ui/buttons/widgets/iconButtons.html'" class="ng-scope" style=""><ul class="btn-list clearfix ng-scope">
                                        <li><button type="button" class="btn btn-primary btn-icon"><i class="ion-android-download"></i></button></li>
                                        <li><button type="button" class="btn btn-default btn-icon"><i class="ion-stats-bars"></i></button></li>
                                        <li><button type="button" class="btn btn-success btn-icon"><i class="ion-android-checkmark-circle"></i></button></li>
                                        <li><button type="button" class="btn btn-info btn-icon"><i class="ion-information"></i></button></li>
                                        <li><button type="button" class="btn btn-warning btn-icon"><i class="ion-android-warning"></i></button></li>
                                        <li><button type="button" class="btn btn-danger btn-icon"><i class="ion-nuclear"></i></button></li>
                                    </ul>

                                    <h5 class="panel-subtitle ng-scope">Buttons with icons</h5>

                                    <ul class="btn-list clearfix ng-scope">
                                        <li><button type="button" class="btn btn-primary btn-with-icon"><i class="ion-android-download"></i>Primary</button></li>
                                        <li><button type="button" class="btn btn-default btn-with-icon"><i class="ion-stats-bars"></i>Default</button></li>
                                        <li><button type="button" class="btn btn-success btn-with-icon"><i class="ion-android-checkmark-circle"></i>Success</button></li>
                                        <li><button type="button" class="btn btn-info btn-with-icon"><i class="ion-information"></i>Info</button></li>
                                        <li><button type="button" class="btn btn-warning btn-with-icon"><i class="ion-android-warning"></i>Warning</button></li>
                                        <li><button type="button" class="btn btn-danger btn-with-icon"><i class="ion-nuclear"></i>Danger</button></li>
                                    </ul></div>
                            </div></div></div>
                    <div ba-panel="" ba-panel-title="Large Buttons" ba-panel-class="with-scroll large-buttons-panel"><div class="panel  with-scroll large-buttons-panel animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Large Buttons</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/ui/buttons/widgets/largeButtons.html' --><div ng-include="'app/pages/ui/buttons/widgets/largeButtons.html'" class="ng-scope" style=""><div class="row btns-row btns-same-width-lg ng-scope">
                                        <div class="col-sm-4 col-xs-6"><button type="button" class="btn btn-primary btn-lg">Primary</button></div>
                                        <div class="col-sm-4 col-xs-6"><button type="button" class="btn btn-success btn-lg">Success</button></div>
                                        <div class="col-sm-4 col-xs-6"><button type="button" class="btn btn-info btn-lg">Info</button></div>
                                        <div class="col-sm-4 col-xs-6"><button type="button" class="btn btn-default btn-lg">Default</button></div>
                                        <div class="col-sm-4 col-xs-6"><button type="button" class="btn btn-warning btn-lg">Warning</button></div>
                                        <div class="col-sm-4 col-xs-6"><button type="button" class="btn btn-danger btn-lg">Danger</button></div>
                                    </div></div>
                            </div></div></div>
                </div>
                <div class="col-md-6">
                    <div ba-panel="" ba-panel-title="Button Dropdowns" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Button Dropdowns</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/ui/buttons/widgets/dropdowns.html' --><div ng-include="'app/pages/ui/buttons/widgets/dropdowns.html'" class="ng-scope" style=""><div class="row btns-row ng-scope">
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-primary  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    Primary <span class="caret"></span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-success  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    Success <span class="caret"></span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-info  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    Info <span class="caret"></span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-default  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    Default <span class="caret"></span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-warning  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    Warning <span class="caret"></span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-danger  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    Danger <span class="caret"></span>
                                                </button>

                                            </div>
                                        </div>
                                    </div>

                                    <h5 class="panel-subtitle ng-scope">Split button dropdowns</h5>

                                    <div class="row btns-row ng-scope">
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-primary">Primary</button>
                                                <button type="button" class="btn btn-primary  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-success">Success</button>
                                                <button type="button" class="btn btn-success  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-info">Info</button>
                                                <button type="button" class="btn btn-info  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-default">Default</button>
                                                <button type="button" class="btn btn-default  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-warning">Warning</button>
                                                <button type="button" class="btn btn-warning  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-6">
                                            <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body="">
                                                <button type="button" class="btn btn-danger">Danger</button>
                                                <button type="button" class="btn btn-danger  dropdown-toggle" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                            </div>
                                        </div>
                                    </div></div>
                            </div></div></div>
                    <div ba-panel="" ba-panel-title="Button Groups" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Button Groups</h3></div><div class="panel-body" ng-transclude="">
                                <!-- ngInclude: 'app/pages/ui/buttons/widgets/buttonGroups.html' --><div ng-include="'app/pages/ui/buttons/widgets/buttonGroups.html'" class="ng-scope" style=""><div class="btn-group-example ng-scope">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-danger">Left</button>
                                            <button type="button" class="btn btn-danger">Middle</button>
                                            <button type="button" class="btn btn-danger">Right</button>
                                        </div>
                                    </div>

                                    <div class="btn-toolbar-example ng-scope">
                                        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                            <div class="btn-group" role="group" aria-label="First group">
                                                <button type="button" class="btn btn-primary">1</button>
                                                <button type="button" class="btn btn-primary">2</button>
                                                <button type="button" class="btn btn-primary">3</button>
                                                <button type="button" class="btn btn-primary">4</button>
                                            </div>
                                            <div class="btn-group" role="group" aria-label="Second group">
                                                <button type="button" class="btn btn-primary">5</button>
                                                <button type="button" class="btn btn-primary">6</button>
                                                <button type="button" class="btn btn-primary">7</button>
                                            </div>
                                            <div class="btn-group" role="group" aria-label="Third group">
                                                <button type="button" class="btn btn-primary">8</button>
                                            </div>
                                        </div>
                                    </div></div>
                            </div></div></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" ba-panel="" ba-panel-title="Progress Buttons" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Progress Buttons</h3></div><div class="panel-body" ng-transclude="">
                            <!-- ngInclude: 'app/pages/ui/buttons/widgets/progressButtons.html' --><div ng-include="'app/pages/ui/buttons/widgets/progressButtons.html'" class="ng-scope" style=""><div class="progress-buttons-container text-center default-text ng-scope">
                                    <div class="row">

                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">fill horizontal</span>
                                            <button progress-button="progressFunction()" class="btn btn-success ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-fill state-loading disabled"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }" style="width: 10.1536%; opacity: 1;"></span></span></button>
                                            <button progress-button="progressFunction()" class="btn btn-success ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-fill disabled state-success"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }" style="width: 100%; opacity: 0;"></span></span></button>
                                            <button progress-button="progressFunction()" class="btn btn-success ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-fill"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">fill vertical</span>
                                            <button progress-button="progressFunction()" pb-direction="vertical" class="btn btn-danger ng-isolate-scope progress-button progress-button-dir-vertical progress-button-style-fill"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">shrink horizontal</span>
                                            <button progress-button="progressFunction()" pb-style="shrink" class="btn btn-warning ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-shrink"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">shrink vertical</span>
                                            <button progress-button="progressFunction()" pb-style="shrink" pb-direction="vertical" class="btn btn-info ng-isolate-scope progress-button progress-button-dir-vertical progress-button-style-shrink"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-angle-bottom <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-angle-bottom" class="btn btn-success ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-angle-bottom"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-angle-top <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-angle-top" class="btn btn-danger ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-angle-top"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-angle-left <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-angle-left" class="btn btn-warning ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-angle-left"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-angle-right <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-angle-right" class="btn btn-info ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-angle-right"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-side-down <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-side-down" class="btn btn-success ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-side-down"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-side-up <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-side-up" class="btn btn-danger ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-side-up"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-side-left <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-side-left" class="btn btn-warning ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-side-left"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-side-right <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-side-right" class="btn btn-info ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-side-right"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">rotate-back <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="rotate-back" class="btn btn-success ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-rotate-back"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">flip-open <br>perspective</span>
                                            <button progress-button="progressFunction()" pb-style="flip-open" class="btn btn-danger ng-isolate-scope progress-button-perspective progress-button progress-button-dir-horizontal progress-button-style-flip-open"><span class="progress-wrap"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">slide-down <br>horizontal</span>
                                            <button progress-button="progressFunction()" pb-style="slide-down" class="btn btn-warning ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-slide-down"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                        <section class="col-md-6 col-lg-3">
                                            <span class="button-title">move-up <br>horizontal</span>
                                            <button progress-button="progressFunction()" pb-style="move-up" class="btn btn-info ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-move-up"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col-md-6">
                                            <span class="button-title">top-line <br>horizontal</span>
                                            <button progress-button="progressFunction()" pb-style="top-line" class="btn btn-success ng-isolate-scope progress-button progress-button-dir-horizontal progress-button-style-top-line"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                        <section class="col-md-6">
                                            <span class="button-title">lateral-lines <br>vertical</span>
                                            <button progress-button="progressFunction()" pb-style="lateral-lines" class="btn btn-info ng-isolate-scope progress-button progress-button-dir-vertical progress-button-style-lateral-lines"><span class="content" ng-transclude=""><span class="ng-scope">Submit</span></span><span class="progress"><span class="progress-inner notransition" ng-style="progressStyles" ng-class="{ notransition: !allowProgressTransition }"></span></span></button>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div></div></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div ba-panel="" ba-panel-title="Editable Rows" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Editable Rows</h3></div><div class="panel-body" ng-transclude="">
                                <div include-with-scope="app/pages/tables/widgets/editableRowTable.html" class="ng-scope"><div class="add-row-editable-table">
                                        <button class="btn btn-primary" ng-click="addUser()">Add row</button>
                                    </div>
                                    <table class="table table-bordered table-hover table-condensed">
                                        <tbody><tr>
                                            <td></td>
                                            <td>Name</td>
                                            <td>Status</td>
                                            <td>Group</td>
                                            <td>Actions</td>
                                        </tr>
                                        <!-- ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope" style="">
                                            <td class="ng-binding">
                                                0
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Esther Vang
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Not set
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          vip
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                1
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Leah Freeman
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Excellent
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          user
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                2
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Mathews Simpson
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Excellent
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          customer
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                3
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Buckley Hopkins
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable editable-empty">
          Not set
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          admin
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                4
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Buckley Schwartz
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Good
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          user
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                5
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Mathews Hopkins
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Not set
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          customer
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                6
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Leah Vang
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Not set
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          user
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                7
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Vang Schwartz
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Not set
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          customer
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                8
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Hopkin Esther
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Good
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          customer
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users --><tr ng-repeat="user in users" class="editable-row ng-scope">
                                            <td class="ng-binding">
                                                9
                                            </td>
                                            <td>
        <span editable-text="user.name" e-name="name" e-form="rowform" e-required="" class="ng-scope ng-binding editable">
          Mathews Schwartz
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.status" e-name="status" e-form="rowform" e-selectpicker="" e-ng-options="s.value as s.text for s in statuses" class="ng-scope ng-binding editable">
          Good
        </span>
                                            </td>
                                            <td class="select-td">
        <span editable-select="user.group" e-name="group" onshow="loadGroups()" e-form="rowform" e-selectpicker="" e-ng-options="g.id as g.text for g in groups" class="ng-scope ng-binding editable">
          vip
        </span>
                                            </td>
                                            <td>
                                                <form editable-form="" name="rowform" ng-show="rowform.$visible" class="form-buttons form-inline ng-pristine ng-valid ng-hide" shown="inserted == user">
                                                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary editable-table-button btn-xs">
                                                        Save
                                                    </button>
                                                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default editable-table-button btn-xs">
                                                        Cancel
                                                    </button>
                                                </form>
                                                <div class="buttons" ng-show="!rowform.$visible">
                                                    <button class="btn btn-primary editable-table-button btn-xs" ng-click="rowform.$show()">Edit</button>
                                                    <button class="btn btn-danger editable-table-button btn-xs" ng-click="removeUser($index)">Delete</button>
                                                </div>
                                            </td>
                                        </tr><!-- end ngRepeat: user in users -->
                                        </tbody></table>
                                </div>
                            </div></div></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div ba-panel="" ba-panel-title="Smart Table With Filtering, Sorting And Pagination" ba-panel-class="with-scroll"><div class="panel  with-scroll animated zoomIn" zoom-in=""><div class="panel-heading clearfix"><h3 class="panel-title">Smart Table With Filtering, Sorting And Pagination</h3></div><div class="panel-body" ng-transclude="">
                                <div include-with-scope="app/pages/tables/widgets/smartTable.html" class="ng-scope"><div class="horizontal-scroll">
                                        <div class="form-group select-page-size-wrap ">
                                            <label>Rows on page
                                                <div class="btn-group bootstrap-select form-control show-tick ng-pristine ng-untouched ng-valid"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" title="10"><span class="filter-option pull-left">10</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open"><ul class="dropdown-menu inner" role="menu"><li data-original-index="2"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">5</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">10</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">15</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">20</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="6"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">25</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="form-control selectpicker show-tick ng-pristine ng-untouched ng-valid" title="Rows on page" selectpicker="" ng-model="smartTablePageSize" ng-options="i for i in [5,10,15,20,25]" tabindex="-98"><option data-hidden="true" disabled="" value="" class="" selected="selected">Rows on page</option><option class="bs-title-option" value="">Rows on page</option><option label="5" value="number:5">5</option><option label="10" value="number:10" selected="selected">10</option><option label="15" value="number:15">15</option><option label="20" value="number:20">20</option><option label="25" value="number:25">25</option></select></div>
                                            </label>
                                        </div>
                                        <table class="table" st-table="smartTableData">
                                            <thead>
                                            <tr class="sortable ">
                                                <th class="table-id st-sort-ascent" st-sort="id" st-sort-default="true">#</th>
                                                <th st-sort="firstName">First Name</th>
                                                <th st-sort="lastName">Last Name</th>
                                                <th st-sort="username">Username</th>
                                                <th st-sort="email">Email</th>
                                                <th st-sort="age">Age</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <th><input st-search="firstName" placeholder="Search First Name" class="input-sm form-control search-input" type="search"></th>
                                                <th><input st-search="lastName" placeholder="Search Last Name" class="input-sm form-control search-input" type="search"></th>
                                                <th><input st-search="username" placeholder="Search Username" class="input-sm form-control search-input" type="search"></th>
                                                <th><input st-search="email" placeholder="Search Email" class="input-sm form-control search-input" type="search">
                                                </th>
                                                <th><input st-search="age" placeholder="Search Age" class="input-sm form-control search-input" type="search">
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <!-- ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope" style="">
                                                <td class="table-id ng-binding">1</td>
                                                <td class="ng-binding">Mark</td>
                                                <td class="ng-binding">Otto</td>
                                                <td class="ng-binding">@mdo</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:mdo@gmail.com" href="mailto:mdo@gmail.com">mdo@gmail.com</a></td>
                                                <td class="ng-binding">28</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">2</td>
                                                <td class="ng-binding">Jacob</td>
                                                <td class="ng-binding">Thornton</td>
                                                <td class="ng-binding">@fat</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:fat@yandex.ru" href="mailto:fat@yandex.ru">fat@yandex.ru</a></td>
                                                <td class="ng-binding">45</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">3</td>
                                                <td class="ng-binding">Larry</td>
                                                <td class="ng-binding">Bird</td>
                                                <td class="ng-binding">@twitter</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:twitter@outlook.com" href="mailto:twitter@outlook.com">twitter@outlook.com</a></td>
                                                <td class="ng-binding">18</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">4</td>
                                                <td class="ng-binding">John</td>
                                                <td class="ng-binding">Snow</td>
                                                <td class="ng-binding">@snow</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:snow@gmail.com" href="mailto:snow@gmail.com">snow@gmail.com</a></td>
                                                <td class="ng-binding">20</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">5</td>
                                                <td class="ng-binding">Jack</td>
                                                <td class="ng-binding">Sparrow</td>
                                                <td class="ng-binding">@jack</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:jack@yandex.ru" href="mailto:jack@yandex.ru">jack@yandex.ru</a></td>
                                                <td class="ng-binding">30</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">6</td>
                                                <td class="ng-binding">Ann</td>
                                                <td class="ng-binding">Smith</td>
                                                <td class="ng-binding">@ann</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:ann@gmail.com" href="mailto:ann@gmail.com">ann@gmail.com</a></td>
                                                <td class="ng-binding">21</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">7</td>
                                                <td class="ng-binding">Barbara</td>
                                                <td class="ng-binding">Black</td>
                                                <td class="ng-binding">@barbara</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:barbara@yandex.ru" href="mailto:barbara@yandex.ru">barbara@yandex.ru</a></td>
                                                <td class="ng-binding">43</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">8</td>
                                                <td class="ng-binding">Sevan</td>
                                                <td class="ng-binding">Bagrat</td>
                                                <td class="ng-binding">@sevan</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:sevan@outlook.com" href="mailto:sevan@outlook.com">sevan@outlook.com</a></td>
                                                <td class="ng-binding">13</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">9</td>
                                                <td class="ng-binding">Ruben</td>
                                                <td class="ng-binding">Vardan</td>
                                                <td class="ng-binding">@ruben</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:ruben@gmail.com" href="mailto:ruben@gmail.com">ruben@gmail.com</a></td>
                                                <td class="ng-binding">22</td>
                                            </tr><!-- end ngRepeat: item in smartTableData --><tr ng-repeat="item in smartTableData" class="ng-scope">
                                                <td class="table-id ng-binding">10</td>
                                                <td class="ng-binding">Karen</td>
                                                <td class="ng-binding">Sevan</td>
                                                <td class="ng-binding">@karen</td>
                                                <td><a class="email-link ng-binding" ng-href="mailto:karen@yandex.ru" href="mailto:karen@yandex.ru">karen@yandex.ru</a></td>
                                                <td class="ng-binding">33</td>
                                            </tr><!-- end ngRepeat: item in smartTableData -->
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="6" class="text-center">
                                                    <div st-pagination="" st-items-by-page="smartTablePageSize" st-displayed-pages="5" class="ng-isolate-scope"><!-- ngIf: numPages && pages.length >= 2 --><nav ng-if="numPages &amp;&amp; pages.length >= 2" class="ng-scope" style=""><ul class="pagination"><!-- ngRepeat: page in pages --><li ng-repeat="page in pages" ng-class="{active: page==currentPage}" class="ng-scope active"><a ng-click="selectPage(page)" class="ng-binding">1</a></li><!-- end ngRepeat: page in pages --><li ng-repeat="page in pages" ng-class="{active: page==currentPage}" class="ng-scope"><a ng-click="selectPage(page)" class="ng-binding">2</a></li><!-- end ngRepeat: page in pages --><li ng-repeat="page in pages" ng-class="{active: page==currentPage}" class="ng-scope"><a ng-click="selectPage(page)" class="ng-binding">3</a></li><!-- end ngRepeat: page in pages --><li ng-repeat="page in pages" ng-class="{active: page==currentPage}" class="ng-scope"><a ng-click="selectPage(page)" class="ng-binding">4</a></li><!-- end ngRepeat: page in pages --><li ng-repeat="page in pages" ng-class="{active: page==currentPage}" class="ng-scope"><a ng-click="selectPage(page)" class="ng-binding">5</a></li><!-- end ngRepeat: page in pages --></ul></nav><!-- end ngIf: numPages && pages.length >= 2 --></div>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div></div>
                            </div></div></div>
                </div>
            </div>

            <div class="modal-buttons same-width clearfix" ng-controller="NotificationsCtrl">
                <button type="button" class="btn btn-success" id="sec_noti">Success Notification</button>
                <button type="button" class="btn btn-info" ng-click="showInfoMsg()">Info Notification</button>
                <button type="button" class="btn btn-warning" ng-click="showWarningMsg()">Warning Notification</button>
                <button type="button" class="btn btn-danger" ng-click="showErrorMsg()">Danger Notification</button>
            </div>


        </div>
        <!--end content -->
    </div>
    <footer class="al-footer clearfix">
        <div class="al-footer-right">Created with <i class="ion-heart"></i></div>
        <div class="al-footer-main clearfix">
            <div class="al-copy">Blur Admin 2016</div>
            <ul class="al-share clearfix">
                <li><i class="socicon socicon-facebook"></i></li>
                <li><i class="socicon socicon-twitter"></i></li>
                <li><i class="socicon socicon-google"></i></li>
                <li><i class="socicon socicon-github"></i></li>
            </ul>
        </div>
    </footer>
    <back-top>
        <i class="fa fa-angle-up back-top white" id="back-top" title="Back to Top" style="right: 40px; bottom: 40px; position: fixed;"></i>
    </back-top>
</main>
<script src="lib/jquery.js"></script>
<script src="lib/angular.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="lib/angular-progress-button-styles.min.js"></script>



<script type="text/javascript">
    $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();

        // fade in #back-top
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });

            // scroll body to 0px on click
            $('#back-top').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });

    });
</script>

</body>
</html>